This project implements a optimizer for Wind Farm Layout problem using Evolutional algorithm. The project is submitted as a requirement for the course "Project", lectured by Prof. Martin Kappes.

Contributors:
	Bui Quang Minh - core components, grid, parameter control, log and stat, advanced operators in mutator, recombinator, parallelization
	Tran Minh Thang - evaluator
	Le Thai Nguyen Hoang - visualization
	To Phan Trung Hieu - some operators in mutator, recombinator and selector

Language: Java

Usage: import the project into Eclipse and run class grp3.wfo.ea.Master to start a new session or grp3.wfo.ea.Recover to recover a session from log file.

Public Git repo: https://bitbucket.org/minhcly95/windfarmoptimization.git

Warning: Offline evaluation is available by using the configuration file in scenario/ folder. Online evaluation is only for the course and currently not available anymore.

Library: Wind Flow Library available in lib folder (windflo.jar)