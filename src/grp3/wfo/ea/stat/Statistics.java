package grp3.wfo.ea.stat;

import java.util.List;

import grp3.wfo.ea.core.struct.MetaOrganism;
import grp3.wfo.ea.gene.General2DGene;

public class Statistics {
	private double avgFitness, avgSize, avgStep;
	private double maxFitness, maxSize, maxStep;
	private double genRate;

	public Statistics(List<MetaOrganism> metaPool) {
		MetaOrganism o = metaPool.get(0);

		avgFitness = metaPool.parallelStream().mapToDouble(mo -> mo.getMetaFitness().getTrueFitness()).average()
				.getAsDouble();
		avgSize = metaPool.parallelStream().mapToDouble(mo -> mo.getOrganism().getSize()).average().getAsDouble();
		avgStep = metaPool.parallelStream().mapToDouble(mo -> mo.getOrganism().getMaxGeneChange()).average()
				.getAsDouble();

		maxFitness = o.getMetaFitness().getTrueFitness();
		maxSize = o.getOrganism().getSize();
		maxStep = o.getOrganism().getMaxGeneChange();

		int numGen = (int) metaPool.parallelStream().filter(mo -> mo.getOrganism().getGene(0) instanceof General2DGene)
				.count();
		genRate = (double) numGen / metaPool.size();
	}

	public void print() {
		System.out.println("\n\tFittest: " + maxFitness + " - " + maxSize + " - " + maxStep);
		System.out.println("\tAverage: " + avgFitness + " - " + avgSize + " - " + avgStep);
		System.out.println("\tGeneralized rate: " + genRate);
	}

	public double getAvgFitness() {
		return avgFitness;
	}
	public double getAvgSize() {
		return avgSize;
	}
	public double getAvgStep() {
		return avgStep;
	}
	public double getMaxFitness() {
		return maxFitness;
	}
	public double getMaxSize() {
		return maxSize;
	}
	public double getMaxStep() {
		return maxStep;
	}
	public double getGenRate() {
		return genRate;
	}
}
