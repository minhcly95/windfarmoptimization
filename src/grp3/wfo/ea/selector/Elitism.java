package grp3.wfo.ea.selector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class Elitism implements Selector {

	private static final long serialVersionUID = -3901488354328193197L;
	
	private int elite;
	private Selector child;
	
	public Elitism(int elite, Selector child) {
		this.elite = elite;
		this.child = child;
	}

	@Override
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count) {
		pool = new ArrayList<>(pool);
		Collections.sort(pool);
		
		List<MetaOrganism> select = new ArrayList<>(pool.subList(0, elite));
		select.addAll(child.select(pool, count - elite));
		
		return select;
	}

}
