package grp3.wfo.ea.selector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class TournamentSelection implements Selector {

	private static final long serialVersionUID = -947813005993579741L;

	private int tournamentSize;
	private int selectCount;

	public TournamentSelection(int tourSize, int selectCount) {
		this.tournamentSize = tourSize;
		this.selectCount = selectCount;
	}

	@Override
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count) {
		List<MetaOrganism> select = new ArrayList<>(count);
		for (int i = 0; i < count / selectCount; i++) {
			List<MetaOrganism> tour = new ArrayList<>(tournamentSize);
			for (int j = 0; j < tournamentSize; j++) {
				tour.add(pool.get(Common.random.nextInt(pool.size())));
			}
			Collections.sort(tour);
			select.addAll(tour.subList(0, selectCount));
		}
		return select;
	}

}
