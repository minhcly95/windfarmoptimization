package grp3.wfo.ea.selector;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class RouletteWheel implements Selector {

	private static final long serialVersionUID = 3875402047655995934L;

	@Override
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count) {
		List<MetaOrganism> select = new ArrayList<>(count);

		double[] cummulative = new double[pool.size()];
		cummulative[0] = pool.get(0).getFitness();

		for (int i = 1; i < pool.size(); i++)
			cummulative[i] = cummulative[i - 1] + pool.get(i).getFitness();
		
		double sum = cummulative[pool.size() - 1];
		double step = sum / count;
		double start = Common.random.nextDouble() * step;

		int j = 0;
		for (double i = start; i < sum; i += step) {
			while (cummulative[j] < i) j++;
			select.add(pool.get(j));
		}

		return select;
	}

}
