package grp3.wfo.ea.selector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class GreedySelection implements Selector {

	private static final long serialVersionUID = 6440848342264637574L;

	@Override
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count) {
		pool= new ArrayList<>(pool);
		Collections.sort(pool);
		
		return new ArrayList<>(pool.subList(0, count));
	}

}
