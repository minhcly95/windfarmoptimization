package grp3.wfo.ea.selector;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class UniformSelection implements Selector {

	private static final long serialVersionUID = -7742948383258326957L;

	@Override
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count) {
		int size = pool.size();
		
		List<MetaOrganism> select = IntStream.range(0, count).parallel()
			.mapToObj(i -> pool.get(Common.random.nextInt(size)))
				.collect(Collectors.toList());
		
		return select;
	}

}
