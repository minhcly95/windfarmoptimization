package grp3.wfo.ea.entry;

import java.io.File;
import java.io.IOException;

import grp3.wfo.ea.core.Population;
import grp3.wfo.ea.evaluator.OnlineEvaluator;
import grp3.wfo.ea.evaluator.WindFarmEvaluator;
import grp3.wfo.ea.log.Logger;
import grp3.wfo.ea.log.Reader;

public final class Recover {
	Population pop;
	
	public Recover() throws IOException, Exception {
		// Old log
		Reader reader = new Reader(new File("./log/onl2"));
		
		// New log
		Logger logger = new Logger(new File("./log/onl2"));

		//WindFarmEvaluator evaluator = new WindFarmEvaluator("./scenarios/obs_00.xml");
		OnlineEvaluator evaluator = new OnlineEvaluator(2);
		
		int gen = reader.getLastGeneration();
		pop = new Population(reader.readConfiguration(), reader.readPool(gen), evaluator, gen, logger);
		System.out.println("Population recovered");
	}
	
	public static void main(String[] args) {
		try {
			Recover m = new Recover();
			m.pop.evolve(100, 2000);
			System.out.println("Program ended");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
