package grp3.wfo.ea.entry;

import java.io.File;
import java.io.IOException;

import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.Population;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.evaluator.OnlineEvaluator;
import grp3.wfo.ea.gene.*;
import grp3.wfo.ea.log.Logger;
import grp3.wfo.ea.mutator.*;
import grp3.wfo.ea.param.ExponentialParameter;
import grp3.wfo.ea.param.Maximum;
import grp3.wfo.ea.recombinator.*;
import grp3.wfo.ea.selector.*;

public final class Master {
	Population pop;

	public Master() throws IOException, Exception {
		Logger logger = new Logger(new File("./log/onl2"));

		//WindFarmEvaluator evaluator = new WindFarmEvaluator("./scenarios/obs_00.xml");
		OnlineEvaluator evaluator = new OnlineEvaluator(2);

		Configuration conf = new Configuration(evaluator.R * 8,
				new double[] { 0, 0, evaluator.width, evaluator.height }, evaluator.obstacles, 50, 0, 0.95);

		Gene sampleGene = new GridGene(conf.getGrid());
		int maxSize = conf.getGrid().getSize();

		pop = new Population(new Maximum(new ExponentialParameter(40, 0.25, 50), 10), sampleGene, maxSize, evaluator,
				conf, logger);

		pop.addMutator(new GeneGeneralization(), 0.01);
		pop.addMutator(new SelfAdaptiveGeneAdding(), 0.5);
		pop.addMutator(new CreepMutation(), 0.25);

		pop.addRecombinator(new RandomSpawn(maxSize, sampleGene), new UniformSelection(), 0.1);
		pop.addRecombinator(new XCrossover(), new UniformSelection(), 0.2);
		pop.addRecombinator(new YCrossover(), new UniformSelection(), 0.2);
		pop.addRecombinator(new SelfAdaptiveGeneRemove(), new UniformSelection(), 0.5);

		pop.setSurvivalSelector(new GreedySelection());

		System.out.println(" - Population initialized");
	}

	public static void main(String[] args) {
		try {
			Master m = new Master();
			m.pop.evolve(200, 300);
			System.out.println("Program ended");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
