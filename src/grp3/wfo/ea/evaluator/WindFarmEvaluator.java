package grp3.wfo.ea.evaluator;

import competition.KusiakLayoutEvaluator;
import competition.scenarios.WindScenario;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Evaluator;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.struct.MetaFitness;

public class WindFarmEvaluator extends WindScenario implements Evaluator {
	
	public WindFarmEvaluator(String fileName) throws Exception {
		super(fileName);
	}
	
	@Override
	public MetaFitness evaluate(Organism o) {
		KusiakLayoutEvaluator localEvaluator = new KusiakLayoutEvaluator();
		localEvaluator.initialize(this);
		
		double[][] layout = new double[o.getSize()][2];
		for (int i = 0; i < o.getSize(); i++){
			Gene g = o.getGene(i);
			layout[i][0] = g.getX();
			layout[i][1] = g.getY();
		}		

		double coe = Double.NEGATIVE_INFINITY;
		localEvaluator.evaluate(layout);
		coe = 1 / localEvaluator.getEnergyCost();
		
		System.out.print('.');
		
		try {
			localEvaluator.getTurbineFitnesses();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new MetaFitness(coe, localEvaluator.getTurbineFitnesses());
	}

	@Override
	public boolean canParallel() {
		return true;
	}
}
