package grp3.wfo.ea.evaluator;

import evaluation.CompetitionEvaluator;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Evaluator;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.struct.MetaFitness;

public class OnlineEvaluator extends CompetitionEvaluator implements Evaluator {
	
	private int scenario;
	
	public OnlineEvaluator(int scenario) {
		this.hostname = "http://windflo.mueller-bady.com:8081";
		this.user_token = "CHXCLZRKJ2XHRV1MWYFTT05K1EVP5X";
		this.run_token = "STJ7L5ZTE3MHRWGUG45KQ44IZMKM4J";
		
		this.scenario = scenario;
		if (run_token.isEmpty())
			this.initialize(scenario, user_token);
		else
			this.initialize(scenario, user_token, run_token);
	}
	
	@Override
	public MetaFitness evaluate(Organism o) {
		double[][] layout = new double[o.getSize()][2];
		for (int i = 0; i < o.getSize(); i++){
			Gene g = o.getGene(i);
			layout[i][0] = g.getX();
			layout[i][1] = g.getY();
		}		
				
		double coe = Double.NEGATIVE_INFINITY;
		
		OnlineEvaluator evaluator = new OnlineEvaluator(scenario);
		evaluator.evaluate(layout);
		coe = 1 / evaluator.getEnergyCost();

		System.out.print('.');
		
		return new MetaFitness(coe, evaluator.getTurbineFitnesses());
	}

	@Override
	public boolean canParallel() {
		return true;
	}
}
