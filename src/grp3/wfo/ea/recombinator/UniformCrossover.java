package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class UniformCrossover implements Recombinator {

	private static final long serialVersionUID = -1935259420213656343L;

	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 2)
			throw new IllegalArgumentException();

		Organism o1 = organisms[0].getOrganism();
		Organism o2 = organisms[1].getOrganism();
		List<Gene> genome = new ArrayList<Gene>();

		for (int i = 0; i < Math.max(o1.getSize(), o2.getSize()); i++) {
			if (i > o1.getSize())
				genome.add(o2.getGene(i));
			if (i > o2.getSize())
				genome.add(o1.getGene(i));

			if (Common.random.nextDouble() < 0.5)
				genome.add(o1.getGene(i));
			else
				genome.add(o2.getGene(i));
		}

		return new Organism(genome, (o1.getMaxGeneChange() + o2.getMaxGeneChange()) / 2);
	}

	@Override
	public int getParentsCount() {
		return 2;
	}

}
