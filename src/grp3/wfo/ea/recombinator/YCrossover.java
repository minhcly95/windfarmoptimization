package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.List;
import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;
import grp3.wfo.ea.gene.General2DGene;

public class YCrossover implements Recombinator {

	private static final long serialVersionUID = -8907378711434115683L;

	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 2)
			throw new IllegalArgumentException();

		Organism o1 = organisms[0].getOrganism();
		Organism o2 = organisms[1].getOrganism();
		boolean convert = !o1.getGene(0).getClass().equals(o2.getGene(0).getClass());
		List<Gene> genome = new ArrayList<>();

		double maxY = Configuration.current.getConstraint().getDimensions()[3];

		double y = Common.random.nextDouble() * maxY;

		for (Gene g : o1.getGenes()) {
			if (g.getY() < y)
				genome.add((convert && !(g instanceof General2DGene)) ? new General2DGene(g.getX(), g.getY()) : g);
		}
		for (Gene g : o2.getGenes()) {
			if (g.getY() >= y && Configuration.current.getConstraint().checkDistances(genome, g))
				genome.add((convert && !(g instanceof General2DGene)) ? new General2DGene(g.getX(), g.getY()) : g);
		}
		return new Organism(genome, (o1.getMaxGeneChange() + o2.getMaxGeneChange()) / 2);
	}

	@Override
	public int getParentsCount() {
		return 2;
	}

}