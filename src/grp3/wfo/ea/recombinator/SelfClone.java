package grp3.wfo.ea.recombinator;

import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class SelfClone implements Recombinator {

	private static final long serialVersionUID = -7024524453153383503L;

	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 1)
			throw new IllegalArgumentException();
		
		return new Organism(organisms[0].getOrganism());
	}

	@Override
	public int getParentsCount() {
		return 1;
	}

}
