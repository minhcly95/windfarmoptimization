package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class CutAndSplice implements Recombinator {

	private static final long serialVersionUID = -6771389491791878565L;

	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 2)
			throw new IllegalArgumentException();
		Organism o1 = organisms[0].getOrganism();
		Organism o2 = organisms[1].getOrganism();
		List<Gene> genome = new ArrayList<>();

		int a = Common.random.nextInt(o1.getSize());
		int b = Common.random.nextInt(o2.getSize());

		genome.addAll(o1.getGenes().subList(0, a));
		genome.addAll(o2.getGenes().subList(b, o2.getSize()));

		return new Organism(genome, (o1.getMaxGeneChange() + o2.getMaxGeneChange()) / 2);
	}

	@Override
	public int getParentsCount() {
		return 2;
	}

}
