package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class GeneRemove implements Recombinator {

	private static final long serialVersionUID = -7024524453153383503L;
	
	private int maxRemove;
	
	public GeneRemove(int maxRemove) {
		this.maxRemove = maxRemove;
	}
	
	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 1)
			throw new IllegalArgumentException();

		int noRemove = Common.random.nextInt(maxRemove) + 1;
		
		if (organisms[0].getOrganism().getSize() <= noRemove)
			return new Organism(organisms[0].getOrganism());
		
		List<Gene> genes = new ArrayList<>(organisms[0].getOrganism().getGenes());
		double[] fitness = organisms[0].getMetaFitness().getGeneFitness();
		
		PriorityQueue<GeneFitnessEntry> entries = new PriorityQueue<>();
		for(int i = 0; i < genes.size(); i++)
			entries.add(new GeneFitnessEntry(genes.get(i), fitness[i]));
		
		for (int i = 0; i < noRemove; i++)
			genes.remove(entries.poll().getGene());

		return new Organism(genes, organisms[0].getOrganism().getMaxGeneChange());
	}

	@Override
	public int getParentsCount() {
		return 1;
	}

	private class GeneFitnessEntry implements Comparable<GeneFitnessEntry> {

		private Gene gene;
		private double fitness;
		
		public GeneFitnessEntry(Gene gene, double fitness) {
			this.gene = gene;
			this.fitness = fitness;
		}
		
		@Override
		public int compareTo(GeneFitnessEntry o) {
			return Double.compare(this.getFitness(), o.getFitness());
		}

		public Gene getGene() {
			return gene;
		}
		public double getFitness() {
			return fitness;
		}
	}
}
