package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class OnePointCrossover implements Recombinator {

	private static final long serialVersionUID = 1581769608571565827L;

	@Override
	public Organism combine(MetaOrganism... organisms) {
		if (organisms.length != 2)
			throw new IllegalArgumentException();
		
		Organism o1 = organisms[0].getOrganism();
		Organism o2 = organisms[1].getOrganism();
		List<Gene> genome = new ArrayList<>();
		
		if (o1.getSize() > o2.getSize()) {
			Organism tmp = o1;
			o1 = o2;
			o2 = tmp;
		}
		int j = Common.random.nextInt(o1.getSize());
			
		for (int i = 0; i < j; i++)
			genome.add(o1.getGene(i));

		for (int i = j; i < o2.getSize(); i++)
			genome.add(o2.getGene(i));

		return new Organism(genome, (o1.getMaxGeneChange() + o2.getMaxGeneChange()) / 2);
	}

	@Override
	public int getParentsCount() {
		return 2;
	}

}
