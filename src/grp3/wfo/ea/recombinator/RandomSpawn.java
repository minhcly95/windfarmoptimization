package grp3.wfo.ea.recombinator;

import java.util.ArrayList;
import java.util.HashSet;
import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class RandomSpawn implements Recombinator {

	private static final long serialVersionUID = -7024524453153383503L;

	private int maxSize;
	private Gene sample;
	
	public RandomSpawn(int maxSize, Gene sample) {
		this.maxSize = maxSize;
		this.sample = sample;
	}
	
	@Override
	public Organism combine(MetaOrganism... organisms) {
		int n = Common.random.nextInt(maxSize) + 1;
		//int n = maxSize;
		
		HashSet<Gene> geneSet = new HashSet<>();
		for (int i = 0; i < n; i++) {
			Gene g = sample.random(geneSet);
			if (g != null)
				geneSet.add(g);
		}
		
		return new Organism(new ArrayList<>(geneSet), Configuration.current.getInitGeneChange());
	}

	@Override
	public int getParentsCount() {
		return 1;
	}

}
