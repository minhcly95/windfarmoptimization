package grp3.wfo.ea.core;

import java.io.Serializable;

import grp3.wfo.ea.grid.Grid;

public class Configuration implements Serializable {
	private static final long serialVersionUID = 3294666440803492514L;

	public static Configuration current;
	
	private Constraint constraint;
	private double[] dimension;
	private double minDistance;
	
	private Grid grid;
	
	private double initGeneChange;
	private double learningRate;
	private double decayRate;
	
	public Configuration(double minDistance, double[] dimension, double[][] obstacles,
			double initGeneChange, double learningRate, double decayRate) {
		this.minDistance = minDistance;
		this.dimension = dimension;
		this.constraint = new Constraint(minDistance, dimension, obstacles);
		this.grid = new Grid(constraint);
		this.initGeneChange = initGeneChange;
		this.learningRate = learningRate;
		this.decayRate = decayRate;
	}

	public double getInitGeneChange() {
		return initGeneChange;
	}
	public double getLearningRate() {
		return learningRate;
	}
	public double getDecayRate() {
		return decayRate;
	}
	public Constraint getConstraint() {
		return constraint;
	}
	public double[] getDimension() {
		return dimension;
	}
	public double getMinDistance() {
		return minDistance;
	}
	public Grid getGrid() {
		return grid;
	}
}
