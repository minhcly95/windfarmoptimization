package grp3.wfo.ea.core;

import java.io.Serializable;
import java.util.Collection;
import grp3.wfo.ea.core.inf.Gene;

public class Constraint implements Serializable {
	
	private static final long serialVersionUID = -7002092327810719509L;
	
	private double[][] obstacles;
	private double minDistance;
	private double[] dimensions;
	
	public Constraint(double minDistance, double[] dimensions, double[][] obstacles) {
		this.minDistance = minDistance;
		this.dimensions = dimensions;
		this.obstacles = obstacles;
	}
	
	public boolean checkObstacles(Gene gene) {
		return checkObstacles(gene.getX(), gene.getY());
	}
	public boolean checkObstacles(double x, double y) {
		for (double[] obs : obstacles)
			if (x >= obs[0] && y >= obs[1] && x <= obs[2] && y <= obs[3])
				return false;
		
		return true;
	}
	
	public boolean checkDimension(Gene gene) {
		return checkDimension(gene.getX(), gene.getY());
	}
	public boolean checkDimension(double x, double y) {
		return x >= dimensions[0] && y >= dimensions[1] && x <= dimensions[2] && y <= dimensions[3];
	}
	
	public boolean checkDistances(Collection<Gene> genome, Gene newGene) {
		double sqrMinDist = minDistance * minDistance;
		for (Gene g : genome)
			if (Gene.sqrDistance(g, newGene) < sqrMinDist)
				return false;
		return true;
	}

	public double[][] getObstacles() {
		return obstacles;
	}
	public double getMinDistance() {
		return minDistance;
	}
	public double[] getDimensions() {
		return dimensions;
	}
}
