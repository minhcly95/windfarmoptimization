package grp3.wfo.ea.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import grp3.wfo.ea.core.inf.Gene;

public class Organism implements Serializable {
	private static final long serialVersionUID = -750843911552583722L;
	
	private ArrayList<Gene> genome;
	private double maxGeneChange;
	
	private Organism() {
		genome = new ArrayList<>();
		maxGeneChange = Configuration.current.getInitGeneChange();
	}
	public Organism(Organism o) {
		genome = new ArrayList<>(o.genome);
		maxGeneChange = o.maxGeneChange;
	}
	public Organism(List<Gene> genes, double maxGeneChange) {
		genome = new ArrayList<>(genes);
		this.maxGeneChange = maxGeneChange;
	}
	@Override
	public Organism clone() {
		return new Organism(this);
	}
	
	public static Organism random(int maxSize, Gene sample) {
		Organism o = new Organism();
		
		int n = Common.random.nextInt(maxSize) + 1;
		//int n = maxSize;
		
		HashSet<Gene> geneSet = new HashSet<>();
		for (int i = 0; i < n; i++)
			geneSet.add(sample.random(geneSet));
		
		o.genome = new ArrayList<>(geneSet);
		
		return o;
	}
	public void step() {
		//maxGeneChange *= Math.exp(Configuration.current.getLearningRate() * Common.random.nextGaussian());
		//maxGeneChange = Math.min(maxGeneChange, 100);
		maxGeneChange *= Configuration.current.getDecayRate();
	}
	
	public int getSize() {
		return genome.size();
	}
	public double getMaxGeneChange() {
		return maxGeneChange;
	}
	public Gene getGene(int i) {
		return genome.get(i);
	}
	public List<Gene> getGenes() {
		return Collections.unmodifiableList(genome);
	}
	
	@Override
	public String toString() {
		return genome.toString();
	}
}
