package grp3.wfo.ea.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import grp3.wfo.ea.core.inf.Evaluator;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Mutator;
import grp3.wfo.ea.core.inf.Parameter;
import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.inf.Selector;
import grp3.wfo.ea.core.struct.MetaMutator;
import grp3.wfo.ea.core.struct.MetaOrganism;
import grp3.wfo.ea.core.struct.MetaRecombinator;
import grp3.wfo.ea.gene.General2DGene;
import grp3.wfo.ea.gene.GridGene;
import grp3.wfo.ea.log.Logger;
import grp3.wfo.ea.selector.UniformSelection;
import grp3.wfo.ea.stat.Statistics;

public class Population implements Serializable {
	private static final long serialVersionUID = 3831258113227290244L;

	private transient List<Organism> spawnPool;
	private transient List<MetaOrganism> metaPool;
	private Parameter size;
	private transient int generation;

	private Configuration configuration;
	private transient Evaluator evaluator;

	private List<MetaMutator> mutators = new ArrayList<>();
	private List<MetaRecombinator> recombinators = new ArrayList<>();
	private Selector selector;

	private transient Logger logger = null;
	private int noEval;

	public Population(Parameter size, Gene sample, int geneCount, Evaluator evaluator, Configuration configuration) {
		Configuration.current = this.configuration = configuration;

		this.size = size;
		this.noEval = 0;
		this.evaluator = evaluator;
		this.selector = new UniformSelection(); // Default survival selector

		spawnPool = new ArrayList<>();
		metaPool = new ArrayList<>();
		this.generation = 0;

		System.out.print("S");
		for (int i = 0; i < size.get(0); i++)
			spawnPool.add(Organism.random(geneCount, sample));

		evaluate();
	}

	public Population(Parameter size, Gene sample, int geneCount, Evaluator evaluator, Configuration configuration,
			Logger logger) {
		this(size, sample, geneCount, evaluator, configuration);
		this.logger = logger;
	}

	// Recover system
	public Population(Population copy, List<MetaOrganism> organism, Evaluator evaluator, int generation) {
		Configuration.current = this.configuration = copy.configuration;

		this.spawnPool = new ArrayList<>();
		this.metaPool = organism;
		this.size = copy.size;
		this.evaluator = evaluator;
		this.mutators = copy.mutators;
		this.recombinators = copy.recombinators;
		this.selector = copy.selector;
		this.generation = generation;
		this.noEval = copy.noEval;
	}

	public Population(Population copy, List<MetaOrganism> organism, Evaluator evaluator, int generation,
			Logger logger) {
		this(copy, organism, evaluator, generation);
		this.logger = logger;
	}

	public MetaMutator addMutator(Mutator mutator, double rate) {
		MetaMutator meta = new MetaMutator(mutator, rate);
		mutators.add(meta);
		return meta;
	}

	public void removeMutator(MetaMutator meta) {
		mutators.remove(meta);
	}

	public MetaRecombinator addRecombinator(Recombinator recombinator, Selector selector, double rate) {
		MetaRecombinator meta = new MetaRecombinator(recombinator, selector, rate);
		recombinators.add(meta);
		return meta;
	}

	public void removeRecombinator(MetaRecombinator meta) {
		recombinators.remove(meta);
	}

	public void setSurvivalSelector(Selector selector) {
		this.selector = selector != null ? selector : new UniformSelection();
	}

	private void spawn() {
		System.out.print("S");
		spawnPool.clear();
		List<MetaOrganism> readOnlyPool = Collections.unmodifiableList(metaPool);

		for (MetaRecombinator r : recombinators) {

			int n = (int) Math.ceil(r.getRate() * getGenerationalSize());
			List<MetaOrganism> matingPool = r.getSelector().select(readOnlyPool, n * r.getParentsCount());

			List<MetaOrganism> gridPool = matingPool.parallelStream()
					.filter(mo -> mo.getOrganism().getGene(0) instanceof GridGene).collect(Collectors.toList());
			List<MetaOrganism> generalPool = matingPool.parallelStream()
					.filter(mo -> mo.getOrganism().getGene(0) instanceof General2DGene).collect(Collectors.toList());

			List<MetaOrganism[]> pairs = new ArrayList<>();

			pairs.addAll(pairFromPool(gridPool, r.getParentsCount()));
			pairs.addAll(pairFromPool(generalPool, r.getParentsCount()));
			
			generalPool.addAll(gridPool);
			pairs.addAll(pairFromPool(generalPool, r.getParentsCount()));

			spawnPool.addAll(
					pairs.parallelStream().map(p -> r.getRecombinator().combine(p)).collect(Collectors.toList()));
		}
	}
	private List<MetaOrganism[]> pairFromPool(List<MetaOrganism> pool, int parentsCount) {
		List<MetaOrganism[]> pairs = new ArrayList<>();
		
		while (pool.size() >= parentsCount) {
			MetaOrganism[] parents = new MetaOrganism[parentsCount];
			for (int j = 0; j < parentsCount; j++) {
				int index = Common.random.nextInt(pool.size());
				parents[j] = pool.get(index);
				pool.remove(index);
			}
			pairs.add(parents);
		}
		
		return pairs;
	}

	private void mutate() {
		System.out.print("M");
		Stream<Organism> stream = spawnPool.parallelStream();

		for (MetaMutator m : mutators) {
			stream = stream.map(o -> Common.random.nextDouble() < m.getRate()
					? new Organism(m.getMutator().apply(o), o.getMaxGeneChange()) : o);
		}

		spawnPool = stream.collect(Collectors.toList());

		spawnPool.parallelStream().forEach(o -> o.step());
		metaPool.parallelStream().forEach(mo -> mo.getOrganism().step());
	}

	private void evaluate() {
		System.out.print("E");
		// metaPool.clear();

		Stream<Organism> stream = evaluator.canParallel() ? spawnPool.parallelStream() : spawnPool.stream();
		noEval += spawnPool.size();
		metaPool.addAll(stream.map(o -> new MetaOrganism(o, evaluator.evaluate(o))).collect(Collectors.toList()));
		
		spawnPool.clear();
	}

	private void filter() {
		System.out.print("F - " + (int)Math.ceil(size.get(generation)));
		metaPool = selector.select(metaPool, getGenerationalSize()).parallelStream().map(mo -> new MetaOrganism(mo))
				.sorted(new Comparator<MetaOrganism>() {
					@Override
					public int compare(MetaOrganism o1, MetaOrganism o2) {
						return -Double.compare(o1.getMetaFitness().getTrueFitness(),
								o2.getMetaFitness().getTrueFitness());
					}
				}).collect(Collectors.toList());
	}

	public void evolve(int numGen, int eval) throws FileNotFoundException, IOException {
		Configuration.current = this.configuration;

		if (logger != null) {
			logger.writeConfiguration(this);
			logger.writePool(generation, metaPool);
		}

		for (generation++; generation <= numGen && noEval + getGenerationalSize() <= eval; generation++) {
			System.out.print("Generation " + generation + ": ");

			spawn();
			mutate();
			evaluate();
			filter();

			new Statistics(metaPool).print();

			if (logger != null)
				logger.writePool(generation, metaPool);
		}
		MetaOrganism o = metaPool.get(0);
		System.out.println(
				o.getFitness() + " - " + o.getOrganism().getSize() + " - " + o.getOrganism().getMaxGeneChange());
	}

	public Constraint getConstraint() {
		return configuration.getConstraint();
	}

	public int getSize() {
		return metaPool.size();
	}

	public List<MetaMutator> getMutators() {
		return mutators;
	}

	public List<MetaRecombinator> getRecombinators() {
		return recombinators;
	}

	public Selector getSurvivalSelector() {
		return selector;
	}
	
	private int getGenerationalSize() {
		return (int)Math.ceil(size.get(generation));
	}

}
