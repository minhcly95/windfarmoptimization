package grp3.wfo.ea.core.inf;

import java.io.Serializable;

public interface Parameter extends Serializable {
	public double get(int gen);
}
