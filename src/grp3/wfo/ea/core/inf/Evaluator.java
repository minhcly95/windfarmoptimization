package grp3.wfo.ea.core.inf;

import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.struct.MetaFitness;

public interface Evaluator {
	public MetaFitness evaluate(Organism o);
	public boolean canParallel();
}
