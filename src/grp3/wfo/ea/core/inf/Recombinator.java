package grp3.wfo.ea.core.inf;

import java.io.Serializable;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.struct.MetaOrganism;

public interface Recombinator extends Serializable {
	public Organism combine(MetaOrganism... organisms);
	public int getParentsCount();
}
