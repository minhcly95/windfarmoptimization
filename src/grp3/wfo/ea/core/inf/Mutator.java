package grp3.wfo.ea.core.inf;

import java.io.Serializable;
import java.util.List;

import grp3.wfo.ea.core.Organism;

public interface Mutator extends Serializable {
	public List<Gene> apply(Organism organism);
}
