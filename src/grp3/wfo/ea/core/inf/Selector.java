package grp3.wfo.ea.core.inf;

import java.io.Serializable;
import java.util.List;

import grp3.wfo.ea.core.struct.MetaOrganism;

public interface Selector extends Serializable {
	public List<MetaOrganism> select(List<MetaOrganism> pool, int count);
}
