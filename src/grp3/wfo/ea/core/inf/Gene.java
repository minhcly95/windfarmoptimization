package grp3.wfo.ea.core.inf;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

public interface Gene extends Serializable {
	public double getX();
	public double getY();
	
	public HashMap<String, Double> getConfiguration();
	
	public Gene step();
	public Gene random(Collection<Gene> gene);
	
	public static double distance(Gene g1, Gene g2) {
		return Math.sqrt(sqrDistance(g1, g2));
	}
	public static double sqrDistance(Gene g1, Gene g2) {
		double deltaX = g1.getX() - g2.getX();
		double deltaY = g1.getY() - g2.getY();
		return deltaX * deltaX + deltaY * deltaY;
	}
}
