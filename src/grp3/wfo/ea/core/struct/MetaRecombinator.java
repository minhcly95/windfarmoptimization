package grp3.wfo.ea.core.struct;

import java.io.Serializable;

import grp3.wfo.ea.core.inf.Recombinator;
import grp3.wfo.ea.core.inf.Selector;

public final class MetaRecombinator implements Serializable {
	private static final long serialVersionUID = -3089628597052907580L;
	
	private Recombinator recombinator;
	private Selector selector;
	private double rate;
	
	public MetaRecombinator(Recombinator recombinator, Selector selector, double rate) {
		this.recombinator = recombinator;
		this.selector = selector;
		this.rate = rate;
	}

	public Recombinator getRecombinator() {
		return recombinator;
	}
	public Selector getSelector() {
		return selector;
	}
	public int getParentsCount() {
		return recombinator.getParentsCount();
	}
	public double getRate() {
		return rate;
	}
}
