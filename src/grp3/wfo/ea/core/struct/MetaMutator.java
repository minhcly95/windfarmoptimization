package grp3.wfo.ea.core.struct;

import java.io.Serializable;

import grp3.wfo.ea.core.inf.Mutator;

public final class MetaMutator implements Serializable {
	private static final long serialVersionUID = 4289292665948494717L;
	
	private Mutator mutator;
	private double rate;
	
	public MetaMutator(Mutator mutator, double rate) {
		this.mutator = mutator;
		this.rate = rate;
	}

	public Mutator getMutator() {
		return mutator;
	}
	public double getRate() {
		return rate;
	}
}
