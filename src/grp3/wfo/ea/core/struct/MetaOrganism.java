package grp3.wfo.ea.core.struct;

import java.io.Serializable;

import grp3.wfo.ea.core.Organism;

public class MetaOrganism implements Comparable<MetaOrganism>, Serializable {
	private static final long serialVersionUID = 8794285301608902002L;
	
	private Organism organism;
	private MetaFitness fitness;
	
	public MetaOrganism(Organism organism, MetaFitness fitness) {
		this.organism = organism;
		this.fitness = fitness;
	}
	public MetaOrganism(MetaOrganism copy) {
		this.organism = new Organism(copy.organism);
		this.fitness = new MetaFitness(copy.fitness);
	}

	public Organism getOrganism() {
		return organism;
	}
	public double getFitness() {
		return fitness.getFitness();
	}
	public MetaFitness getMetaFitness() {
		return fitness;
	}
	
	@Override
	public int compareTo(MetaOrganism o) {
		return Double.compare(o.getFitness(), this.getFitness());
	}
}
