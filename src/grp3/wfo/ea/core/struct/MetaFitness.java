package grp3.wfo.ea.core.struct;

import java.io.Serializable;

public class MetaFitness implements Serializable {

	private static final long serialVersionUID = 4953705566272933335L;

	private double trueFitness;
	private double fitness;
	private double[] geneFitness;
	
	public MetaFitness(double fitness, double[] geneFitness) {
		this.fitness = this.trueFitness = fitness;
		this.geneFitness = geneFitness;
	}
	public MetaFitness(MetaFitness copy) {
		this.fitness = copy.fitness;
		this.trueFitness = copy.trueFitness;
		this.geneFitness = copy.geneFitness;
	}
	
	public double getFitness() {
		return fitness;
	}
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}
	public double getTrueFitness() {
		return trueFitness;
	}
	public double[] getGeneFitness() {
		return geneFitness;
	}
}
