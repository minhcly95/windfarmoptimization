package grp3.wfo.ea.param;

import grp3.wfo.ea.core.inf.Parameter;

public class ConstantParameter implements Parameter {
	
	private static final long serialVersionUID = -1947331779184701894L;
	
	double value;
	
	public ConstantParameter(double value) {
		this.value = value;
	}
	
	@Override
	public double get(int gen) {
		return value;
	}
}
