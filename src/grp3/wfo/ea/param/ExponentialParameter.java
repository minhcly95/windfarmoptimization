package grp3.wfo.ea.param;

import grp3.wfo.ea.core.inf.Parameter;

public class ExponentialParameter implements Parameter {

	private static final long serialVersionUID = 7921851013396240631L;
	
	double initValue;
	double factor;

	public ExponentialParameter(double initValue, double factor) {
		this.initValue = initValue;
		this.factor = factor;
	}
	public ExponentialParameter(double initValue, double factor, double after) {
		this.initValue = initValue;
		this.factor = Math.pow(factor, 1 / after);
	}
	
	@Override
	public double get(int gen) {
		return initValue * Math.pow(factor, gen);
	}
}
