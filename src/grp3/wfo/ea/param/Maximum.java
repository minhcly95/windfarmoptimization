package grp3.wfo.ea.param;

import grp3.wfo.ea.core.inf.Parameter;

public class Maximum implements Parameter {
	
	private static final long serialVersionUID = 2471041704454845942L;
	
	Parameter param1, param2;
	
	public Maximum(Parameter param1, Parameter param2) {
		this.param1 = param1;
		this.param2 = param2;
	}
	public Maximum(Parameter param1, double constant) {
		this.param1 = param1;
		this.param2 = new ConstantParameter(constant);
	}
	
	@Override
	public double get(int gen) {
		return Math.max(param1.get(gen), param2.get(gen));
	}
}
