package grp3.wfo.ea.grid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grp3.wfo.ea.core.Constraint;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.gene.General2DGene;

public class Grid implements Serializable {

	private static final long serialVersionUID = -370392532421573196L;

	private List<General2DGene> points;
	private List<List<Integer>> neighbors;

	public Grid(Constraint constraint) {
		double horizontalStep = constraint.getMinDistance();
		double verticalStep = constraint.getMinDistance() * Math.sqrt(0.75) + 0.001;
		double oddEvenStep = horizontalStep / 2;
		double[] dim = constraint.getDimensions();
		boolean odd = false;

		points = new ArrayList<>();
		neighbors = new ArrayList<>();

		List<Integer> lastRow = new ArrayList<>();
		List<Integer> currentRow = new ArrayList<>();

		double minDist = constraint.getMinDistance();

		for (double v = dim[1]; v < dim[3]; v += verticalStep) {
			for (double h = dim[0] + (odd ? oddEvenStep : 0); h < dim[2]; h += horizontalStep)
				if (constraint.checkObstacles(h, v)) {
					int index = points.size();

					currentRow.add(index);
					points.add(new General2DGene(h, v));
					neighbors.add(new ArrayList<>());

					if (index > 0
							&& Gene.sqrDistance(points.get(index), points.get(index - 1)) <= minDist * minDist + 1) {
						neighbors.get(index).add(index - 1);
						neighbors.get(index - 1).add(index);
					}
					for (int i : lastRow)
						if (Gene.sqrDistance(points.get(index), points.get(i)) <= minDist * minDist + 1) {
							neighbors.get(index).add(i);
							neighbors.get(i).add(index);
						}
				}
			odd = !odd;

			lastRow = currentRow;
			currentRow = new ArrayList<>();
		}
	}

	public int getSize() {
		return points.size();
	}

	public General2DGene getPoint(int index) {
		return points.get(index);
	}
	public List<Integer> getNeighbors(int index) {
		return Collections.unmodifiableList(neighbors.get(index));
	}
}
