package grp3.wfo.ea.visualization;

import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.sun.xml.internal.ws.policy.jaxws.SafePolicyReader;

import grp3.wfo.ea.core.Population;
import grp3.wfo.ea.core.struct.MetaOrganism;
import grp3.wfo.ea.log.Reader;
import grp3.wfo.ea.stat.Statistics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.NullPointerException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

public class LayoutViewer extends JFrame implements ActionListener {

	private static final long serialVersionUID = -3668930879907696920L;
	private JFrame frame;
	private JTextField txtGeneration;
	private JTextField txtOrganism;
	protected int num1 = -1, num2 = -1;
	ArrayList<JFrame> disposeFrame = new ArrayList<JFrame>();

	Reader reader = null;
	Population pop = null;
	List<MetaOrganism> metaPool = null;
	Statistics stat;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LayoutViewer window = new LayoutViewer();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LayoutViewer() throws IOException {
		getContentPane().setLayout(null);
		initialize();
	}

	private void initialize() throws IOException, NullPointerException {
		// Make GUI
		frame = new JFrame();
		frame.setBounds(100, 100, 527, 285);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblLayoutViewer = new JLabel("Layout Viewer");
		lblLayoutViewer.setBounds(159, 0, 145, 57);
		lblLayoutViewer.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.getContentPane().add(lblLayoutViewer);

		txtGeneration = new JTextField();
		txtGeneration.setHorizontalAlignment(SwingConstants.CENTER);
		txtGeneration.setText("Generation");
		txtGeneration.setBounds(81, 66, 175, 25);
		frame.getContentPane().add(txtGeneration);
		txtGeneration.setColumns(10);

		txtOrganism = new JTextField();
		txtOrganism.setHorizontalAlignment(SwingConstants.CENTER);
		txtOrganism.setText("Organism");
		txtOrganism.setBounds(334, 66, 175, 25);
		frame.getContentPane().add(txtOrganism);
		txtOrganism.setColumns(10);

		JLabel GeneAtt = new JLabel("Generation Attributes");
		GeneAtt.setVerticalAlignment(SwingConstants.TOP);
		GeneAtt.setHorizontalAlignment(SwingConstants.LEFT);
		GeneAtt.setBounds(174, 99, 335, 139);
		frame.getContentPane().add(GeneAtt);

		JCheckBox chckbxKeepOpened = new JCheckBox("Keep opened");
		chckbxKeepOpened.setSelected(true);
		chckbxKeepOpened.setBounds(0, 213, 113, 25);
		frame.getContentPane().add(chckbxKeepOpened);

		ActionListener genChanged = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				txtGeneration.setText(Integer.toString(num1));
				try {
					metaPool = reader.readPool(num1);
					stat = new Statistics(metaPool);
					GeneAtt.setText("<html>Gen: " + num1 + "<br>AvgFitness: " + stat.getAvgFitness() + "<br>AvgSize: "
							+ stat.getAvgSize() + "<br>AvgStep: " + stat.getAvgStep() + "<br>MaxFitness: "
							+ stat.getMaxFitness() + "<br>MaxSize: " + stat.getMaxSize() + "<br>MaxStep: "
							+ stat.getMaxStep() + "<br>GenRate: " + stat.getGenRate() + "<html>");

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		ActionListener orgChanged = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				txtOrganism.setText(Integer.toString(num2));
				num1 = Integer.parseInt(txtGeneration.getText());
				num2 = Integer.parseInt(txtOrganism.getText());
				if (!chckbxKeepOpened.isSelected()) {
					if (disposeFrame.size() == 2) {
						for (int i = 0; i < disposeFrame.size(); i++) {
							disposeFrame.get(i).dispose();
						}
						disposeFrame.remove(0);
					} else if (disposeFrame.size() > 2) {
						JFrame savedFrame = disposeFrame.get(disposeFrame.size() - 1);
						for (int i = 0; i < disposeFrame.size(); i++) {
							disposeFrame.get(i).dispose();
						}
						disposeFrame.clear();
						disposeFrame.add(0, savedFrame);
					}
				}
				try {
					metaPool = reader.readPool(num1);
					disposeFrame.add(
							new Visualization(reader.readPool(num1).get(num2), pop, "Gen " + num1 + " - Org " + num2)
									.getCanvas());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		};

		JButton btnInc = new JButton("+");
		btnInc.addActionListener(genChanged);
		btnInc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				num1 += 1;
				if (num1 > reader.getLastGeneration())
					num1 = 0;
			}
		});
		btnInc.setBounds(0, 66, 41, 25);
		frame.getContentPane().add(btnInc);

		JButton btnInc1 = new JButton("+");
		btnInc1.addActionListener(orgChanged);
		btnInc1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 += 1;
				if (num2 >= metaPool.size())
					num2 = 0;
			}
		});
		btnInc1.setBounds(255, 66, 41, 25);
		frame.getContentPane().add(btnInc1);

		JButton btnDec = new JButton("-");
		btnDec.addActionListener(genChanged);
		btnDec.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				num1 -= 1;
				if (num1 < 0)
					num1 = reader.getLastGeneration();
			}
		});
		btnDec.setBounds(41, 66, 41, 25);
		frame.getContentPane().add(btnDec);

		JButton btnDec1 = new JButton("-");
		btnDec1.addActionListener(orgChanged);
		btnDec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				num2 -= 1;
				if (num2 < 0)
					num2 = metaPool.size() - 1;
				;
			}
		});
		btnDec1.setBounds(295, 66, 41, 25);
		frame.getContentPane().add(btnDec1);

		JButton btnFolder = new JButton("Folder");
		JFrame self = this;

		btnFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select log folder");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(self) == JFileChooser.APPROVE_OPTION) {
					chooser.getCurrentDirectory();
					chooser.getSelectedFile();
					try {
						reader = new Reader(chooser.getSelectedFile());
						pop = reader.readConfiguration();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		btnFolder.setBounds(0, 38, 82, 25);
		frame.getContentPane().add(btnFolder);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

	}
}
