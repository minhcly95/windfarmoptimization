package grp3.wfo.ea.visualization;

import java.awt.*;
import grp3.wfo.ea.core.*;
import grp3.wfo.ea.core.struct.*;
import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Visualization extends Canvas {

	private static final long serialVersionUID = -98870320211240493L;
	double width, height, fitness;
	double scale = 19;
	
	Organism o;
	double[][] obstacles;
	double[] turbineFitnesses;
	
	String title;

	public Visualization(MetaOrganism org, Population pop, String title) throws FileNotFoundException, IOException {
		width = pop.getConstraint().getDimensions()[2];
		height = pop.getConstraint().getDimensions()[3];
		obstacles = pop.getConstraint().getObstacles();

		this.o = org.getOrganism();
		turbineFitnesses = org.getMetaFitness().getGeneFitness();
		fitness = org.getFitness();

		setSize((int) (width / scale), (int) (height / scale));
		
		this.title = title;
	}

	public JFrame getCanvas() throws FileNotFoundException, IOException {
		JFrame canvas = new JFrame();
		// Draw the layout
		canvas.setLayout(new BorderLayout());
		canvas.setSize(this.getSize());
		canvas.setTitle(title);
		canvas.add(new Label("Fitness: " + fitness), BorderLayout.NORTH);
		canvas.add(this, BorderLayout.CENTER);
		canvas.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		canvas.setLocationRelativeTo(null);
		canvas.setResizable(false);
		canvas.pack();
		canvas.setVisible(true);
		return canvas;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.GRAY);
		for (double[] dim : obstacles) {
			g.fillRect((int) (dim[0] / scale), (int) (dim[1] / scale), (int) ((dim[2] - dim[0]) / scale),
					(int) ((dim[3] - dim[1]) / scale));
		}

		int minDist = (int) (308 / scale);

		for (int i = 0; i < o.getSize(); i++) {

			g.setColor(Color.getHSBColor((float) turbineFitnesses[i] * 2f / 3, 1f, 1f));
			g.fillOval((int) (o.getGene(i).getX() / scale) - minDist / 2,
					(int) (o.getGene(i).getY() / scale) - minDist / 2, minDist, minDist);

			g.setColor(Color.BLACK);
			g.drawOval((int) (o.getGene(i).getX() / scale) - minDist / 2,
					(int) (o.getGene(i).getY() / scale) - minDist / 2, minDist, minDist);
			g.fillOval((int) (o.getGene(i).getX() / scale) - 1, (int) (o.getGene(i).getY() / scale) - 1, 3, 3);

		}
	}

}
