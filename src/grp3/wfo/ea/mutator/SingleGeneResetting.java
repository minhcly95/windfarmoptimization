package grp3.wfo.ea.mutator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Mutator;

public class SingleGeneResetting implements Mutator {
	
	private static final long serialVersionUID = 4399393062904577512L;

	@Override
	public List<Gene> apply(Organism o) {
		List<Gene> genome = new ArrayList<>(o.getGenes());
		
		int i = Common.random.nextInt(o.getSize());
		Gene g = genome.get(i);
		genome.set(i, null);
		genome.set(i, g.random(genome));
		
		return genome;
	}

}
