package grp3.wfo.ea.mutator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Mutator;

public class WholeResetting implements Mutator {

	private static final long serialVersionUID = -3738328311847367106L;

	@Override
	public List<Gene> apply(Organism o) {
		List<Gene> genome = new ArrayList<>();
		
		for (Gene g : o.getGenes())
			genome.add(g.random(genome));
		
		return genome;
	}

}
