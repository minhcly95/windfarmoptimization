package grp3.wfo.ea.mutator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Mutator;

public class GeneAdding implements Mutator {

	private static final long serialVersionUID = -3724826019252593654L;

	private int maxAppend;
	
	public GeneAdding(int maxAppend) {
		this.maxAppend = maxAppend;
	}
	
	@Override
	public List<Gene> apply(Organism o) {
		Gene sample = o.getGene(0);
		HashSet<Gene> genome = new HashSet<>(o.getGenes());
		
		int noAppend = Common.random.nextInt(maxAppend) + 1;
		
		for (int i = 0; i < noAppend && genome.size() < Configuration.current.getGrid().getSize(); i++)
			genome.add(sample.random(genome));
		
		return new ArrayList<>(genome);
	}
}
