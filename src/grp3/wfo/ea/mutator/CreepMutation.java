package grp3.wfo.ea.mutator;

import java.util.ArrayList;
import java.util.List;

import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Mutator;

public class CreepMutation implements Mutator {

	private static final long serialVersionUID = 9082805839224856347L;
	private static final int RESET_TIMES = 25;

	@Override
	public List<Gene> apply(Organism o) {
		List<Gene> source = o.getGenes();
		List<Gene> genome = new ArrayList<>();

		for (int i = 0; i < o.getSize(); i++) {
			List<Gene> reference = new ArrayList<>(genome);
			reference.addAll(source.subList(i + 1, source.size()));
			Gene g = null;
			
			int reset;
			for (reset = 0; reset < RESET_TIMES; reset++) {
				g = o.getGene(i).step();
				if (Configuration.current.getConstraint().checkDistances(reference, g))
					break;
			}
			
			if (reset >= RESET_TIMES)
				genome.add(o.getGene(i));
			else
				genome.add(g);
		}
		
		return genome;
	}

}
