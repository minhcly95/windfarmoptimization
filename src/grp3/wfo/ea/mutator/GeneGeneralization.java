package grp3.wfo.ea.mutator;

import java.util.List;
import java.util.stream.Collectors;

import grp3.wfo.ea.core.Organism;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.core.inf.Mutator;
import grp3.wfo.ea.gene.General2DGene;

public class GeneGeneralization implements Mutator {

	private static final long serialVersionUID = -5317629399235300826L;

	@Override
	public List<Gene> apply(Organism o) {
		return o.getGenes().stream().map(g -> new General2DGene(g.getX(), g.getY())).collect(Collectors.toList());
	}

}
