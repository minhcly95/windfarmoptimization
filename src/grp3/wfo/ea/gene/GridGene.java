package grp3.wfo.ea.gene;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.inf.Gene;
import grp3.wfo.ea.grid.Grid;

public final class GridGene implements Gene {
	
	private static final long serialVersionUID = -9194026508036414634L;
	
	private int index;
	private Grid grid;
	
	public double getX() {
		return grid.getPoint(index).getX();
	}
	public double getY() {
		return grid.getPoint(index).getY();
	}
	
	public GridGene(Grid grid) {
		this.grid = grid;
	}
	private GridGene() {}
	
	public Gene random(Collection<Gene> genome) {
		GridGene g = new GridGene();
		g.grid = grid;
		
		do {
			g.index = Common.random.nextInt(grid.getSize());
		} while (genome.contains(g));
		
		return g;
	}
	public Gene step() {
		GridGene g = new GridGene();
		g.grid = grid;
		
		List<Integer> neighbors = grid.getNeighbors(index);
		g.index = neighbors.get(Common.random.nextInt(neighbors.size()));
		
		return g;
	}
	
	@Override
	public String toString() {
		return "<" + index + ">";
	}
	
	@Override
	public boolean equals(Object obj) {
		try {
			GridGene g = (GridGene)obj;
			return g.index == this.index;
		} catch (Exception e) {
			return false;
		}
	}
	@Override
	public int hashCode() {
		return index;
	}
	
	@Override
	public HashMap<String, Double> getConfiguration() {
		return null;
	}
}
