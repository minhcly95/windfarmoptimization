package grp3.wfo.ea.gene;

import java.util.Collection;
import java.util.HashMap;
import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.inf.Gene;

public final class Uncorrelated2DGene implements Gene {
	private static final long serialVersionUID = -6784888589271368516L;
	
	private double x;
	private double y;

	private double sdX;
	private double sdY;
	
	private transient HashMap<String, Double> props;
	
	public static final String INIT_STEP = "initStep";
	public static final String LEARNING_RATE = "learningRate";
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getSdX() {
		return sdX;
	}
	public double getSdY() {
		return sdY;
	}
	
	public Uncorrelated2DGene(HashMap<String, Double> props) {
		this.props = new HashMap<>(props);
	}
	private Uncorrelated2DGene() {}
	private Uncorrelated2DGene(Uncorrelated2DGene g) {
		this.x = g.x;
		this.y = g.y;
		this.sdX = g.sdX;
		this.sdY = g.sdY;
		this.props = g.props;
	}
	
	public Gene random(Collection<Gene> genome) {
		Uncorrelated2DGene g = new Uncorrelated2DGene();
		g.props = props;
		
		double[] dim = Configuration.current.getDimension();
		
		do {
			g.x = Common.random.nextDouble() * (dim[2] - dim[0]) + dim[0];
			g.y = Common.random.nextDouble() * (dim[3] - dim[1]) + dim[1];
		} while (!Configuration.current.getConstraint().checkObstacles(g.x, g.y) ||
				!Configuration.current.getConstraint().checkDistances(genome, g));
		
		g.sdX = g.sdY = props.get(INIT_STEP);
		
		return g;
	}
	public Gene step() {
		Uncorrelated2DGene g = new Uncorrelated2DGene(this);
		
		double new_x, new_y;
		do {
			new_x = g.x + Common.random.nextGaussian() * g.sdX;
			new_y = g.y + Common.random.nextGaussian() * g.sdY;
		} while (!Configuration.current.getConstraint().checkDimension(new_x, new_y) ||
				!Configuration.current.getConstraint().checkObstacles(new_x, new_y));

		g.x = new_x;
		g.y = new_y;
		
		g.sdX *= Math.exp(Common.random.nextGaussian() * props.get(LEARNING_RATE));
		g.sdY *= Math.exp(Common.random.nextGaussian() * props.get(LEARNING_RATE));
		
		return g;
	}
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	@Override
	public HashMap<String, Double> getConfiguration() {
		return props;
	}
}
