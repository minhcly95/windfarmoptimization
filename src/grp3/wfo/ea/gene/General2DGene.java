package grp3.wfo.ea.gene;

import java.util.Collection;
import java.util.HashMap;
import grp3.wfo.ea.core.Common;
import grp3.wfo.ea.core.Configuration;
import grp3.wfo.ea.core.inf.Gene;

public final class General2DGene implements Gene {
	private static final long serialVersionUID = -6784888589271368516L;
	
	private double x;
	private double y;
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	
	public General2DGene(double x, double y) {
		this.x = x;
		this.y = y;
	}
	private General2DGene() {}
	
	public Gene random(Collection<Gene> genome) {
		General2DGene g = new General2DGene();
		
		double[] dim = Configuration.current.getDimension();
		
		int limit = 0;
		do {
			if (limit >= 50)
				return null;
			
			g.x = Common.random.nextDouble() * (dim[2] - dim[0]) + dim[0];
			g.y = Common.random.nextDouble() * (dim[3] - dim[1]) + dim[1];
			
			limit++;
		} while (!Configuration.current.getConstraint().checkObstacles(g.x, g.y) ||
				!Configuration.current.getConstraint().checkDistances(genome, g));
		
		return g;
	}
	public Gene step() {
		double minDist = Configuration.current.getMinDistance();
		double new_x, new_y;
		
		do {
			new_x = this.x + Common.random.nextGaussian() * minDist;
			new_y = this.y + Common.random.nextGaussian() * minDist;
		} while (!Configuration.current.getConstraint().checkDimension(new_x, new_y) ||
				!Configuration.current.getConstraint().checkObstacles(new_x, new_y));
		
		return new General2DGene(new_x, new_y);
	}
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	@Override
	public HashMap<String, Double> getConfiguration() {
		return null;
	}
}
