package grp3.wfo.ea.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;

import grp3.wfo.ea.core.Population;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class Reader {
	File folder;
	
	public Reader(File folder) throws FileNotFoundException {
		this.folder = folder;
		
		if (!folder.exists() && !folder.isDirectory())
			throw new FileNotFoundException();
	}
	
	public Population readConfiguration() throws FileNotFoundException, IOException {
		File confFile = new File(folder, "pop.conf");
		
		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(confFile));
		Population pop = null;
		try {
			pop = (Population)stream.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		stream.close();
		return pop;
	}
	@SuppressWarnings("unchecked")
	public List<MetaOrganism> readPool(int gen) throws FileNotFoundException, IOException {

		File logFile = new File(folder, String.format("%1$03d.gen", gen));
		
		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(logFile));
		List<MetaOrganism> pool = null;
		try {
			pool = (List<MetaOrganism>) stream.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		stream.close();
		return pool;
	}
	
	public int getLastGeneration() {
		return Arrays.stream(folder.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches("^\\d{3}\\.gen$");
			}
		})).mapToInt(str -> Integer.parseInt(str.substring(0, 3))).max().getAsInt();
	}
}
