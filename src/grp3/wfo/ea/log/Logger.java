package grp3.wfo.ea.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import grp3.wfo.ea.core.Population;
import grp3.wfo.ea.core.struct.MetaOrganism;

public class Logger {
	File folder;
	
	public Logger(File folder) throws IOException {
		this.folder = folder;
		
		if (!folder.exists())
			folder.mkdirs();

		File logFile = new File(folder, "out.log");
		logFile.createNewFile();
		
		FileOutputStream logStream = new FileOutputStream(logFile, true);
		OutputStream systemOut = System.out;
		
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		    	try {
					logStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		});
		System.setOut(new PrintStream(new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				logStream.write(b);
				logStream.flush();
				systemOut.write(b);
				systemOut.flush();
			}
		}));
	}
	
	public void writeConfiguration(Population population) throws FileNotFoundException, IOException {
		File confFile = new File(folder, "pop.conf");
		confFile.createNewFile();
		
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(confFile, false));
		stream.writeObject(population);
		stream.flush();
		stream.close();
	}
	
	public void writePool(int gen, List<MetaOrganism> pool) throws FileNotFoundException, IOException {
		File logFile = new File(folder, String.format("%1$03d.gen", gen));
		logFile.createNewFile();
		
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(logFile, false));
		stream.writeObject(pool);
		stream.flush();
		stream.close();
	}
}
